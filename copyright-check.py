#!/usr/bin/python3

from glob import glob
import re
import warnings

from debian import copyright


# debian.copyright.Copyright is noisy about files it doesn't like
warnings.simplefilter("ignore")


def is_gpl2only(copy):
    """ check if a copyright file reports GPLv2-only

    returns:
        True if there is a Files stanza with "GPL-2" in it.

    raises:
        NotMachineReadableError if the file is not in copyright-format/1.0
        ValueError if it can't figure it out
    """
    # track whether there was a confusing stanza
    unsure = False

    for para in copy:
        # only look at Files and Header paragraphs (not orphaned License paras)
        if 'License' in para and ('Files' in para or 'Upstream' in para):
            try:
                if check_license_gpl2only(para.license.synopsis):
                    return True
            except ValueError:
                unsure = True

    if unsure:
        raise ValueError("Can't figure out license")

    return False


_gplv2_re = re.compile(r"\bGPL-2( |$)")


def check_license_gpl2only(spec):
    # simplest case first
    spec = spec.strip()
    if spec == "GPL-2":
        return True

    if "GPL-2" in spec.split(" and "):
        return True

    # Check to see if GPL-2 is in there anywhere
    if not _gplv2_re.search(spec):
        return False

    options = set(spec.split(" or "))
    allowed = {
        "Apache-2.0",
        "BSD-2-clause",
        "BSD-3-clause",
        "Expat",
        "LGPL-2",
        "LGPL-2.1",
        "LGPL-2.1+",
        "LGPL-3",
        "LGPL-3+",
        "GPL-2+",
        "GPL-3",
        "MPL-1.1",
        "MIT",
        }
    if allowed.intersection(options):
        return False

    # No optional licenses have allowed an escape, so these ones
    # have been used on their own and so are GPLv2only.
    for lic in [
        "gpl-2 with classpath exception",
        "gpl-2 with dune exception",
        "gpl-2 with openssl exception",
        "gpl-2 with qt exception",
    ]:
        if lic in spec.lower():
            return True

    # at this stage it's sufficiently complicated to give up
    raise ValueError("Can't understand specification")


with open("gplv2-only.txt", "w") as fg, open("unknown.txt", "w") as fd:

    for copyfile in sorted(glob("copyright/*")):
        package = copyfile.split("/")[1]

        with open(copyfile) as cf:
            try:
                copy = copyright.Copyright(cf)
                if is_gpl2only(copy):
                    print(package, file=fg)
                    result = "GPL2 stanza"
                else:
                    result = "OK"
            except (copyright.NotMachineReadableError, ValueError):
                print(package, file=fd)
                result = "dunno"

        print("%-40s\t%s" % (package, result))
