#!/usr/bin/python3

import os.path
import pickle
import requests
import yaml


# We will fetch copyright files from the ftp-master data export
base = "http://metadata.ftp-master.debian.org/changelogs/"

# reading in the 36MB filelist.yaml from ftp-master takes forever;
# cache it in a pickle and load it instead to preserve sanity
if os.path.exists("filelist.pickle"):
    print("Reading filelist.pickle")
    with open("filelist.pickle", 'rb') as fh:
        index = pickle.load(fh)
else:
    print("Reading filelist.yaml")
    with open("filelist.yaml") as fh:
        index = yaml.load(fh)
    with open("filelist.pickle", 'wb') as fh:
        pickle.dump(index, fh)

# list of problematic packages from
# https://lists.debian.org/debian-devel/2018/02/msg00227.html
with open("cups_revbuildeps.txt") as fh:
    packages = fh.readlines()
    packages = [p.strip() for p in packages]

# Track the packages for which no changelog could be found;
# a handful of source packages don't have debian/copyright in them
# and so these files must be manually retrieved either from the source
# package (as debian/copyright.in or similar) or from a binary package.
failed = []

for p in packages:
    print(p, end="...", flush=True)
    target = os.path.join("copyright", p)
    if os.path.exists(target):
        print(" skipped")
        continue

    resources = [r for r in index[p]['unstable'] if 'copyright' in r]
    if not resources:
        # one of those packages without a copyright file, it seems
        failed.append(p)
        continue

    # fetch the copyright file from ftp-master
    path = resources[0]
    url = base + path
    r = requests.get(url)
    with open(target, 'wb') as fh:
        fh.write(r.text.encode("UTF-8"))

    print(" .")

print("==== The following packages failed; "
      "fetch manually from source or binary packages")
print("\n".join(failed))
